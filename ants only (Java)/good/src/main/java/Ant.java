import java.util.Date;
import java.util.Random;

public class Ant extends Thread {
    private Random randomGenerator = new Random();

    private ISimulator parentSimulator;
    private Location position;
    private Date birthDateTime  = new Date();
    private Boolean isCarryingFood;
    private Boolean isDead;

    public Ant(ISimulator parentSimulator, Location initialPosition) {
        this.parentSimulator = parentSimulator;
        this.position = initialPosition;
    }

    public Location getPosition() { return position; }
    public Date getBirthDateTime() { return birthDateTime; }
    public Boolean getCarryingFood() { return isCarryingFood; }
    public Boolean getIsDead() { return isDead; }

    @Override
    public void run() {
        while (!isGoingToDie() && parentSimulator.isRunning()) {
            move();
            pickupFood();
        }
        isDead = true;
    }

    private void move() { /* implementation not shown */ }

    private Boolean isGoingToDie() {
        double averageLifeInSeconds = 7200;                     // Average life in seconds (simulated time)
        return (getAgeInSecond()> randomGenerator.nextGaussian()* averageLifeInSeconds);
    }

    private long getAgeInSecond() { return (new Date().getTime() - birthDateTime.getTime())/1000; }

    private void pickupFood() {
        if (!isCarryingFood && position.containsFood()) {
            // Some implementation details not shown
            isCarryingFood = true;
        }
    }
}
