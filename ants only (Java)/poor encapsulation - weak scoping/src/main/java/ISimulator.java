public interface ISimulator {

    void start();
    void stop();
    boolean isRunning();

}
